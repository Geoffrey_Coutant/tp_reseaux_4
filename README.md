# TP_Reseaux_4

## I. First steps

### Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP

- Discord
- Youtube
- Facebook
- Teams
- Netflix

| Nom de l'application | Protocole | IP et port du serveur distant | Port local ouvert |
|----------------------|-----------|-------------------------------|-------------------|
|       Discord        |    UDP    |     162.159.133.234:443       |        62347      |
|       Youtube        |    TCP    |     142.250.74.238:443        |        62391      |
|       Facebook       |    UDP    |     17.252.13.8:3484          |        63184      |
|        Teams         |    TCP    |     52.114.74.220:443         |        63129      |
|       Netflix        |    TCP    |     198.38.120.133:443        |        63231      |

### Demandez l'avis à votre OS

***Discord:***
```
% lsof -i -P
Discord   24766 radiou22   26u  IPv4 0xe58d34af6a4b8587      0t0  TCP 10.33.16.254:62347->162.159.133.234:443 (ESTABLISHED)
```
***Youtube:***
```
% lsof -i -P
Google    30612 radiou22   21u  IPv4 0xe58d34af6a4a5097      0t0  TCP 10.33.16.254:62391->123.35.104.34.bc.googleusercontent.com:80 (CLOSE_WAIT)
```
***facebook:***
```
% lsof -i -P
Google    30612 radiou22   40u  IPv4 0xe58d34af6d534327      0t0  TCP 10.33.16.254:63184->edge-star-shv-01-cdg2.facebook.com:443 (ESTABLISHED)
```
***teams:***
```
% lsof -i -P
Microsoft 35210 radiou22   44u  IPv4 0xe58d34af6a4a3a77      0t0  TCP 10.33.16.254:63129->52.114.74.220:443 (ESTABLISHED)
```
***netflix:***
```
% lsof -i -P
Google    30612 radiou22   41u  IPv4 0xe58d34af6d126457      0t0  TCP 10.33.16.254:63231->ipv4-c196-cdg001-ix.1.oca.nflxvideo.net:443 (CLOSE_WAIT)

```

## II. Mise en place

### 1. SSH

***Examinez le trafic dans Wireshark***

J'ai lancer wireshark ensuite je me suis connecter en SSH **ssh user1@192.168.212.11** et déconnecter pour récupérer les données dans wireshark.

***Demandez aux OS***



### 2. Routage

